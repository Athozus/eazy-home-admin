package com.sivarajan931.eazy_home_admin_client

import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class Settings : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val mySettings: SharedPreference = SharedPreference(this)
        val b1 = findViewById<Button>(R.id.button)
        val cb = findViewById<CheckBox>(R.id.checkbox)
        val eText = findViewById<EditText>(R.id.editText)
        var ipText: String? = null
        try {
            ipText = mySettings.getValueString("ip") as String
            val check = mySettings.getValueString("prefix") as String
            cb.isChecked = check == "https"
            if (ipText != null) {
                eText.setText(ipText)
            }
        } catch (e: Exception) {

        }
        b1.setOnClickListener {
            if (eText.text != null) {
                ipText = eText.getText().toString()
                mySettings.save("ip", ipText!!)

            }
        }
        cb.setOnClickListener {
            if (cb.isChecked) {
                mySettings.save("prefix", "http")
            } else {
                mySettings.save("prefix", "https")
            }

        }

    }
}