package com.sivarajan931.eazy_home_admin_client

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException


class FaceVerificationClient : AppCompatActivity() {
    var b: Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_face_verificaion_client)
        val captureButton = findViewById<ImageButton>(R.id.button2)
        val btnLockDoor = findViewById<ImageButton>(R.id.button3)
        val mQueue = Volley.newRequestQueue(this@FaceVerificationClient)
        btnLockDoor.setOnClickListener{
            val mySettings = SharedPreference(this)
            var devIP: String?
            if (mySettings.getValueString("ip") != null)
                devIP = mySettings.getValueString("ip")
            else devIP = "192.168.0.100"
            val prefix = if (mySettings.getValueString("prefix") != null)
                mySettings.getValueString("prefix")
            else "http"
            val url = "$prefix://$devIP/api/lock_door"

            val jsonObjectRequest = JsonObjectRequest(
                com.android.volley.Request.Method.GET, url, null,
                { response ->
                    val msg = response.getString("msg")
                    Toast.makeText(this@FaceVerificationClient, "$msg", Toast.LENGTH_LONG)
                        .show()
                },
                { error ->
                    // TODO: Handle error
                    Toast.makeText(this@FaceVerificationClient, "Internal Error: $error", Toast.LENGTH_LONG)
                        .show()
                }
            )
            mQueue.add(jsonObjectRequest)

            // Toast.makeText(this@MainScreen, msg, Toast.LENGTH_SHORT).show()

        }
        captureButton.setOnClickListener {
            val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(i, 90)
        }
        findViewById<ImageButton>(R.id.button).setOnClickListener {
            uploadToServer()
        }
    }

    private fun uploadToServer() {
        val c = OkHttpClient()
        val stream = ByteArrayOutputStream()
        b!!.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val byteArray = stream.toByteArray()
        val requestBody: MultipartBody = MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("image", "test.img", RequestBody.create("image/*jpg".toMediaTypeOrNull(), byteArray)).build() //MediaType.parse()
        val mySettings: SharedPreference = SharedPreference(this)
        var devIP: String? = null
        if (mySettings.getValueString("ip") != null)
            devIP = mySettings.getValueString("ip")
        else devIP = "192.168.0.100"
        val prefix = if (mySettings.getValueString("prefix") != null)
            mySettings.getValueString("prefix")
        else "http"
        val r = Request.Builder().url("$prefix://$devIP/api/im_verify").post(requestBody).build()
        c.newCall(r).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

                Toast.makeText(this@FaceVerificationClient, "Error", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call, response: Response) {

                try {
                    val s = response.body!!.string()
                    try {
                        val jsonObject = JSONObject(s)
                        val msg = jsonObject.getString("face")
                        runOnUiThread {
                            if (msg == "Unknown") Toast.makeText(this@FaceVerificationClient, "Access Denied", Toast.LENGTH_LONG).show()
                            else Toast.makeText(this@FaceVerificationClient, "Verified As $msg", Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        //TODO(): SHOW ERROR
                    }
                } catch (e: Exception) {
                    Log.e("Error", e.toString())
                    Toast.makeText(this@FaceVerificationClient, "Error:" + e.message, Toast.LENGTH_LONG).show()
                }

            }
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 90 && resultCode == RESULT_OK) {
            if (data != null) {
                b = data.extras?.get("data") as Bitmap
                findViewById<ImageView>(R.id.imageView).setImageBitmap(b)
                findViewById<ImageButton>(R.id.button).isEnabled= true
            }

        }
    }

}