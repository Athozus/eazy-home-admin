# This script will setup the requirements for running the program on debian based distro.
# run it on debian / any debian based distro.
# Debian based distros are bascially Raspbian, Ubuntu and Linux mint etc...

echo "Installing All dependencies... Please answer yes to all."
sudo apt install -y python3-pip openssl python autoconf pkgconf gcc flex patch make m4 cmake automake libatlas-base-dev libgtk-3-dev libboost-all-dev
sudo pip3 install -r requirements.txt

echo "Please configure your telegram-send by typing telegram-send --configure."
echo "Please upload the testArduinoCode sketch to the arduino remember to configure the server by looking at the readme file".
