# This script will setup the requirements for running the program on arch based distro.
# run it on Manjaro/any Arch based distro
# Arch based distros are Manjaro, Garuda Linux etc.
echo "Installing All dependencies... Please answer yes to all."

sudo pacman -S blas cblas openssl python autoconf pkgconf gcc flex patch make guile m4 cmake python-pip automake arduino arduino-avr-core avrdude arduino-cli base-devel --needed --noconfirm 

sudo pip install -r requirements.txt

echo "Please configure your telegram-send by typing telegram-send --configure"

echo "Please upload the testArduinoCode sketch to the arduino remember to configure the server by looking at the readme file"
